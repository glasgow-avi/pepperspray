﻿using BackgroundTaskComponent;
using BackgroundTaskComponent.DataModel;
using Microsoft.WindowsAzure.MobileServices;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using Windows.Devices.Geolocation;
using Windows.Foundation;
using Windows.Media.SpeechRecognition;
using Windows.Storage;
using Windows.System.UserProfile;

namespace Quickstart_Sending_Local_Toast
{
    class Speech
    {
        // SpeechSynthesizer _synthesizer;                             // The speech synthesizer (text-to-speech, TTS) object
        static SpeechRecognizer _recognizer;                               // The speech recognition object
        static IAsyncOperation<SpeechRecognitionResult> _recoOperation;    // Used to canel the current asynchronous speech recognition operation

        private static bool _recoEnabled = false;                                  // When this is true, we will continue to recognize 
        private static Dictionary<string, int> distressBuzzWords;          // Dictionary of all colors we will recognize and their equivalent color brushes
        private static int distressLevels = 0;

        public Speech()
        {
            //Set up the dictionary of SolidColorBrush objects. 
            if (distressBuzzWords == null)
            {
                distressBuzzWords = new Dictionary<string, int>();
                distressBuzzWords.Add("help", 2);
                distressBuzzWords.Add("let go", 3);
                distressBuzzWords.Add("help me", 3);
                distressBuzzWords.Add("let go of me", 4);
                distressBuzzWords.Add("rape", 5);
                distressBuzzWords.Add("save", 2);
                distressBuzzWords.Add("leave", 1);
                distressBuzzWords.Add("leave me", 3);
                distressBuzzWords.Add("harassing", 5);
                distressBuzzWords.Add("harass", 4);
            }

            if (_recognizer == null)
            {
                _recognizer = new SpeechRecognizer(SpeechRecognizer.SystemSpeechLanguage);

                // Set up a list of colors to recognize.

                _recognizer.Constraints.Add(new SpeechRecognitionListConstraint(distressBuzzWords.Keys));
            }
        }

        private static MobileServiceClient MobileService = new MobileServiceClient(
            "https://pepperspray.azurewebsites.net"
        );

        private static MobileServiceCollection<TodoItem, TodoItem> items;

#if OFFLINE_SYNC_ENABLED
        private IMobileServiceSyncTable<TodoItem> todoTable = App.MobileService.GetSyncTable<TodoItem>(); // offline sync
#else
        private static IMobileServiceTable<TodoItem> todoTable = MobileService.GetTable<TodoItem>();
#endif

        private static async Task InsertTodoItem(TodoItem todoItem)
        {
            // This code inserts a new TodoItem into the database. After the operation completes
            // and the mobile app backend has assigned an id, the item is added to the CollectionView.
            await todoTable.InsertAsync(todoItem);
            items.Add(todoItem);

#if OFFLINE_SYNC_ENABLED
            await App.MobileService.SyncContext.PushAsync(); // offline sync
#endif
        }

        public Library Library = new Library();

        public static void Record_Click()
        {
            if (Library.getRecording())
            {
                Library.Stop();                
            }
            else
            {
                Library.Record();                
            }
        }

        public static void Play_Click()
        {
            Library.Play();
        }

        public static async Task startSpeechRecognition()
        { 
            // Change the button text. 
            if (_recoEnabled)
            {
                _recoEnabled = false;
                distressLevels = 0;

                // Cancel the outstanding recognition operation, if one exists

                Debug.WriteLine("Stopped Speech Recognition. ");

                if (_recoOperation != null && _recoOperation.Status == AsyncStatus.Started)
                {
                    _recoOperation.Cancel();
                }
            }
            else
                // Set the flag to say that we are in recognition mode
                _recoEnabled = true;

            // Continuously recognize speech until the user has canceled 
            while (_recoEnabled)
            {

                // Start recognition.
                await _recognizer.CompileConstraintsAsync();
                SpeechRecognitionResult speechRecognitionResult = await _recognizer.RecognizeAsync();


                // Do something with the recognition result.
                try
                {
                    distressLevels += distressBuzzWords[speechRecognitionResult.Text];
                    if (distressLevels >= 5)
                    {
                        ChangeLockScreenBackground();
                        string recepients;
                        Windows.Storage.StorageFolder storageFolder =
                                Windows.Storage.ApplicationData.Current.LocalFolder;
                        Windows.Storage.StorageFile sampleFile =
                            await storageFolder.GetFileAsync("mynumber.txt");
                        recepients = await Windows.Storage.FileIO.ReadTextAsync(sampleFile);
                        recepients += ";";
                        Windows.Storage.StorageFolder storageFolder1 =
                                Windows.Storage.ApplicationData.Current.LocalFolder;
                        Windows.Storage.StorageFile sampleFile1 =
                            await storageFolder.GetFileAsync("yournumber.txt");
                        string[] all_recepients = (await Windows.Storage.FileIO.ReadTextAsync(sampleFile1)).Split('{');
                        Debug.WriteLine("Found " + all_recepients.Length + " recepients.");
                        Windows.Storage.StorageFile sampleFile3 =
                            await storageFolder.GetFileAsync("custoM Message.txt");
                        string remaining_Message = await Windows.Storage.FileIO.ReadTextAsync(sampleFile3);
                        remaining_Message += " at ";
                        var geolocator = new Geolocator();
                        geolocator.DesiredAccuracyInMeters = 100;
                        Geoposition position = await geolocator.GetGeopositionAsync();
                        Geocoordinate geocoordinate = position.Coordinate;
                        remaining_Message += geocoordinate.Latitude + ", " + geocoordinate.Longitude;
                        remaining_Message += ".";
                        for(int i = 0; i < 3; i++)
                        {
                            Debug.WriteLine(recepients + all_recepients[i] + '}' + remaining_Message);
                            await InsertTodoItem(new TodoItem { Text = recepients + all_recepients[i] + '}' + remaining_Message });
                        }
                        await startSpeechRecognition();
                    }
                    Debug.WriteLine(speechRecognitionResult.Text);
                } catch(Exception)
                {
                    Debug.Write("No keywords found matching string");
                }
            }
        }

        private static async Task ChangeLockScreenBackground()
        {
            if (UserProfilePersonalizationSettings.IsSupported())
            {
                StorageFile file = await StorageFile.GetFileFromApplicationUriAsync(new Uri("ms-appx:///Assets/green.png"));
                UserProfilePersonalizationSettings settings = UserProfilePersonalizationSettings.Current;
                await settings.TrySetLockScreenImageAsync(file);
            }
        }

        private static void ContinuousRecognitionSession_ResultGenerated1(SpeechContinuousRecognitionSession sender, SpeechContinuousRecognitionResultGeneratedEventArgs args)
        {
            // The garbage rule will not have a tag associated with it, the other rules will return a string matching the tag provided
            // when generating the grammar.
            string tag = "unknown";
            Debug.WriteLine(args.Result.Text);
            if (args.Result.Constraint != null)
            {
                tag = args.Result.Constraint.Tag;
                
            }
        }

        /// <summary>
        /// Handle events fired when a result is generated. This may include a garbage rule that fires when general room noise
        /// or side-talk is captured (this will have a confidence of Rejected typically, but may occasionally match a rule with
        /// low confidence).
        /// </summary>
        /// <param name="sender">The Recognition session that generated this result</param>
        /// <param name="args">Details about the recognized speech</param>
        private async void ContinuousRecognitionSession_ResultGenerated(SpeechContinuousRecognitionSession sender, SpeechContinuousRecognitionResultGeneratedEventArgs args)
        {
            
        }
    }
}
