﻿using BackgroundTaskComponent.DataModel;
using Microsoft.QueryStringDotNET;
using Microsoft.WindowsAzure.MobileServices;
using NotificationsExtensions.Toasts;
using Quickstart_Sending_Local_Toast;
using RecordingUtils;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.Background;
using Windows.Foundation;
using Windows.UI.Notifications;
using Windows.UI.Popups;
using Windows.ApplicationModel;

namespace BackgroundTaskComponent
{
    public sealed class ToastNotificationBackgroundTask : IBackgroundTask
    {
        /// <summary>
        /// Initializes the singleton application object.  This is the first line of authored code
        /// executed, and as such is the logical equivalent of main() or WinMain().
        /// </summary>
        private static MobileServiceClient MobileService = new MobileServiceClient(
            "https://pepperspray.azurewebsites.net"
        );

        private static MobileServiceCollection<TodoItem, TodoItem> items;

#if OFFLINE_SYNC_ENABLED
        private IMobileServiceSyncTable<TodoItem> todoTable = App.MobileService.GetSyncTable<TodoItem>(); // offline sync
#else
        private static IMobileServiceTable<TodoItem> todoTable = MobileService.GetTable<TodoItem>();
#endif
        
        private static async Task InsertTodoItem(TodoItem todoItem)
        {
            // This code inserts a new TodoItem into the database. After the operation completes
            // and the mobile app backend has assigned an id, the item is added to the CollectionView.
            await todoTable.InsertAsync(todoItem);
            items.Add(todoItem);

#if OFFLINE_SYNC_ENABLED
            await App.MobileService.SyncContext.PushAsync(); // offline sync
#endif
        }

        private static async Task RefreshTodoItems()
        {
            MobileServiceInvalidOperationException exception = null;
            try
            {
                // This code refreshes the entries in the list view by querying the TodoItems table.
                // The query excludes completed TodoItems.
                items = await todoTable
                    .Where(todoItem => todoItem.Complete == false)
                    .ToCollectionAsync();
            }
            catch (MobileServiceInvalidOperationException e)
            {
                exception = e;
            }

            if (exception != null)
            {
                await new MessageDialog(exception.Message, "Error loading items").ShowAsync();
            }
            else
            {
                //Use items here
            }
        }

        private async Task UpdateCheckedTodoItem(TodoItem item)
        {
            // This code takes a freshly completed TodoItem and updates the database.
            // After the MobileService client responds, the item is removed from the list.
            await todoTable.UpdateAsync(item);
            items.Remove(item);


#if OFFLINE_SYNC_ENABLED
            await App.MobileService.SyncContext.PushAsync(); // offline sync
#endif
        }

        public async void Run(IBackgroundTaskInstance taskInstance)
        {
            // Get a deferral since we're executing async code
            var deferral = taskInstance.GetDeferral();

            try
            {
                // If it's a toast notification action
                if (taskInstance.TriggerDetails is ToastNotificationActionTriggerDetail)
                {
                    // Get the toast activation details
                    var details = taskInstance.TriggerDetails as ToastNotificationActionTriggerDetail;

                    // Deserialize the arguments received from the toast activation
                    QueryString args = QueryString.Parse(details.Argument);

                    // Depending on what action was taken...
                    switch (args["action"])
                    {
                        // User clicked the call button
                        case "call":
                            await HandleCall(details, args);
                            break;
                        // User clicked the find groups button
                        case "find":
                            await HandleFindButton(details, args);
                            break;
                        case "panic":

                            await HandlePanicButton(details, args);
                            break;
                        default:
                            throw new NotImplementedException();
                    }
                }

                // Otherwise handle other background activations
                else
                    throw new NotImplementedException();
            }

            finally
            {
                // And finally release the deferral since we're done
                deferral.Complete();
            }
        }

        private async Task HandleReply(ToastNotificationActionTriggerDetail details, QueryString args)
        {
            // Get the conversation the toast is about
            int conversationId = int.Parse(args["conversationId"]);

            // Get the message that the user typed in the toast
            string message = (string)details.UserInput["tbReply"];

            // In a real app, this would be making a web request, sending the new message
            await Task.Delay(TimeSpan.FromSeconds(2.3));

            // In a real app, you most likely should NOT notify your user that the request completed (only notify them if there's an error)
            SendToast("Your message has been sent! Your message: " + message);
        }

        private async Task HandleFindButton(ToastNotificationActionTriggerDetail details, QueryString args)
        {
            Debug.WriteLine("Recording stopped ");
            // Speech.Record_Click();
            // Speech.Play_Click();
            Debug.WriteLine("Recording played ");
        }

        private async Task HandlePanicButton(ToastNotificationActionTriggerDetail details, QueryString args)
        {
            new Speech();
            await Speech.startSpeechRecognition();
        }

        private async Task HandleCall(ToastNotificationActionTriggerDetail details, QueryString args)
        {
            Windows.ApplicationModel.Calls.PhoneCallManager.ShowPhoneCallUI("+919591327175", "Avi Dutta"); 
        }

        /// <summary>
        /// Simple method to show a basic toast with a message.
        /// </summary>
        /// <param name="message"></param>
        private void SendToast(string message)
        {
            ToastContent content = new ToastContent()
            {
                Visual = new ToastVisual()
                {
                    TitleText = new ToastText()
                    {
                        Text = "Background Task Completed"
                    },

                    BodyTextLine1 = new ToastText()
                    {
                        Text = message
                    }
                }
            };

            ToastNotificationManager.CreateToastNotifier().Show(new ToastNotification(content.GetXml()));
        }
    }
}
