﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using System.Threading.Tasks;
using Windows.UI.Notifications;
using NotificationsExtensions.Toasts;
using Microsoft.QueryStringDotNET;
using Microsoft.WindowsAzure.MobileServices;
using BackgroundTaskComponent.DataModel;
using Windows.UI.Popups;
using System.Xml;
using Windows.Data.Xml.Dom;
using System.Diagnostics;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Pepperspray
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    /// 

    class FileReader
    {
        public static string textFile;

        public static async void writeFile(string text, string fileName)
        {
            Windows.Storage.StorageFolder storageFolder =
                Windows.Storage.ApplicationData.Current.LocalFolder;
            Windows.Storage.StorageFile sampleFile =
                await storageFolder.CreateFileAsync(fileName,
                    Windows.Storage.CreationCollisionOption.ReplaceExisting);
            await Windows.Storage.FileIO.WriteTextAsync(sampleFile, text);

        }

        public static async Task readFile(string fileName)
        {
            try
            {
                Windows.Storage.StorageFolder storageFolder =
                    Windows.Storage.ApplicationData.Current.LocalFolder;
                Windows.Storage.StorageFile sampleFile =
                    await storageFolder.GetFileAsync(fileName);
                textFile = await Windows.Storage.FileIO.ReadTextAsync(sampleFile);
            } catch (Exception)
            {
                textFile = "";
            }


        }
    }

    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();

            getTextBoxes();
        }

        private async void getTextBoxes()
        {
            await FileReader.readFile("mynumber.txt");
            MyNumberTextbox.Text = FileReader.textFile;
            await FileReader.readFile("yournumber.txt");
            BigNumberTextbox.Text = FileReader.textFile.Split('{')[0];
            BigNumber2Textbox.Text = FileReader.textFile.Split('{')[1];
            BigNumber3Textbox.Text = FileReader.textFile.Split('{')[2];
            await FileReader.readFile("custoM Message.txt");
            MessageTextbox.Text = FileReader.textFile;
        }

        private static MobileServiceClient MobileService = new MobileServiceClient(
            "https://pepperspray.azurewebsites.net"
        );

        private static MobileServiceCollection<TodoItem, TodoItem> items;

#if OFFLINE_SYNC_ENABLED
        private IMobileServiceSyncTable<TodoItem> todoTable = App.MobileService.GetSyncTable<TodoItem>(); // offline sync
#else
        private static IMobileServiceTable<TodoItem> todoTable = MobileService.GetTable<TodoItem>();
#endif

        private static async Task InsertTodoItem(TodoItem todoItem)
        {
            // This code inserts a new TodoItem into the database. After the operation completes
            // and the mobile app backend has assigned an id, the item is added to the CollectionView.
            await todoTable.InsertAsync(todoItem);
            items.Add(todoItem);

#if OFFLINE_SYNC_ENABLED
            await App.MobileService.SyncContext.PushAsync(); // offline sync
#endif
        }

        private static async Task RefreshTodoItems()
        {
            MobileServiceInvalidOperationException exception = null;
            try
            {
                // This code refreshes the entries in the list view by querying the TodoItems table.
                // The query excludes completed TodoItems.
                items = await todoTable
                    .Where(todoItem => todoItem.Complete == false)
                    .ToCollectionAsync();
            }
            catch (MobileServiceInvalidOperationException e)
            {
                exception = e;
            }

            if (exception != null)
            {
                await new MessageDialog(exception.Message, "Error loading items").ShowAsync();
            }
            else
            {
                //Use items here
            }
        }

        private async Task UpdateCheckedTodoItem(TodoItem item)
        {
            // This code takes a freshly completed TodoItem and updates the database.
            // After the MobileService client responds, the item is removed from the list.
            await todoTable.UpdateAsync(item);
            items.Remove(item);


#if OFFLINE_SYNC_ENABLED
            await App.MobileService.SyncContext.PushAsync(); // offline sync
#endif
        }

        private void ButtonSendToast_Click(object sender, RoutedEventArgs e)
        {
            // In a real app, these would be initialized with actual data
            string title = "Pepperspray";
            string content = "Pepperspray is ready";
            int conversationId = 384928;

            ToastVisual visual = new ToastVisual()
            {
                TitleText = new ToastText()
                {
                    Text = title
                },

                BodyTextLine1 = new ToastText()
                {
                    Text = content
                },

                /*InlineImages =
                {
                    new ToastImage()
                    {
                        Source = new ToastImageSource(image)
                    }
                },

                AppLogoOverride = new ToastAppLogo()
                {
                    Source = new ToastImageSource(logo),
                    Crop = ToastImageCrop.Circle
                } */
            };

            // Construct the actions for the toast (inputs and buttons)
            ToastActionsCustom actions = new ToastActionsCustom()
            {
                /*
                Inputs =
                {
                    new ToastTextBox("tbReply")
                    {
                        PlaceholderContent = "Type a response"
                    }
                },
                */

                Buttons =
                {
                    /*new ToastButton("Reply", new QueryString()
                    {
                        { "action", "reply" },
                        { "conversationId", conversationId.ToString() }

                    }.ToString())
                    {
                        ActivationType = ToastActivationType.Background,
                        ImageUri = "Assets/Reply.png",

                        // Reference the text box's ID in order to
                        // place this button next to the text box
                        TextBoxId = "tbReply"
                    },

                    */
                    new ToastButton("Call", new QueryString()
                    {
                        { "action", "call" },
                        { "conversationId", conversationId.ToString() }

                    }.ToString())
                    {
                        ActivationType = ToastActivationType.Background
                    },

                    new ToastButton("Find Crowd", new QueryString()
                    {
                        { "action", "find" },
                        { "conversationId", conversationId.ToString() }

                    }.ToString())
                    {
                        ActivationType = ToastActivationType.Background
                    },

                    new ToastButton("Panic", new QueryString()
                    {
                        { "action", "panic" },
                        { "conversationId", conversationId.ToString() }

                    }.ToString())
                    {
                        ActivationType = ToastActivationType.Background
                    }
                }
            };

            ToastContent toastContent = new ToastContent()
            {
                Visual = visual,
                Actions = actions,


                Scenario = ToastScenario.Alarm,

                Launch = new QueryString()
                {
                    { "action", "viewConversation" },
                    { "conversationId", conversationId.ToString() }

                }.ToString()
            };
            // And create the toast notification
            ToastNotification notification = new ToastNotification(toastContent.GetXml());
            // And then send the toast
            ToastNotificationManager.CreateToastNotifier().Show(notification);
        }

        private void saveButton_Click(object sender, RoutedEventArgs e)
        {
            FileReader.writeFile(MyNumberTextbox.Text, "mynumber.txt");
            FileReader.writeFile(BigNumberTextbox.Text + "{" + BigNumber2Textbox.Text + "{" + BigNumber3Textbox.Text, "yournumber.txt");
            FileReader.writeFile(MessageTextbox.Text, "custoM Message.txt");
        }

        private async void checkmessagesButton_Click(object sender, RoutedEventArgs e)
        {
            await RefreshTodoItems();
            foreach (TodoItem item in items)
            {
                string stri = item.Text;
                string senderererer = stri.Split(';')[0];
                string receiver = stri.Split(';')[1].Split('}')[0];
                if (!MyNumberTextbox.Text.Equals(receiver))
                     continue;
                string message = stri.Split('}')[1];
                string[] location = stri.Split(',');
                string longitude = location[location.Length - 1].Substring(1, location[location.Length - 1].Length - 1);
                string latitude = location[0].Split(' ')[location[0].Split(' ').Length - 1];
                ShowToastNotification(senderererer + ": " + message + " " + "http://bing.com/maps/default.aspx?cp=" + latitude + "~" + longitude);
            }
        }

        private void ShowToastNotification(string message)
        {
            ToastTemplateType toastTemplate = ToastTemplateType.ToastImageAndText01;
            Windows.Data.Xml.Dom.XmlDocument toastXml = ToastNotificationManager.GetTemplateContent(toastTemplate);

            // Set Text
            Windows.Data.Xml.Dom.XmlNodeList toastTextElements = toastXml.GetElementsByTagName("text");
            toastTextElements[0].AppendChild(toastXml.CreateTextNode(message));

            // Set image
            // Images must be less than 200 KB in size and smaller than 1024 x 1024 pixels.
            Windows.Data.Xml.Dom.XmlNodeList toastImageAttributes = toastXml.GetElementsByTagName("image");
            ((Windows.Data.Xml.Dom.XmlElement)toastImageAttributes[0]).SetAttribute("src", "ms-appx:///Images/logo-80px-80px.png");
            ((Windows.Data.Xml.Dom.XmlElement)toastImageAttributes[0]).SetAttribute("alt", "logo");

            // toast duration
            IXmlNode toastNode = toastXml.SelectSingleNode("/toast");
            ((Windows.Data.Xml.Dom.XmlElement)toastNode).SetAttribute("duration", "short");

            // toast navigation
            var toastNavigationUriString = "#/MainPage.xaml?param1=12345";
            var toastElement = ((Windows.Data.Xml.Dom.XmlElement)toastXml.SelectSingleNode("/toast"));
            toastElement.SetAttribute("launch", toastNavigationUriString);

            // Create the toast notification based on the XML content you've specified.
            ToastNotification toast = new ToastNotification(toastXml);

            // Send your toast notification.
            ToastNotificationManager.CreateToastNotifier().Show(toast);
        }
    }
}
